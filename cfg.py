""" global configuration variables """

# settings
wsport = 8881

# tcp connection to bonsai
bonsai_host = 'localhost'
bonsai_port = 50008
bonsai_buffersize = 1024
bonsai_connect_retry_maxtime = 60.
bonsai_connect_retry_sleep = 2.
bonsai_socket_timeout = 5.  # seconds

# dummy mode: just run websocket server, return dummy data to gui
dummy_mode = False

# connection object reference
commander = False

# health status
# starts as True and is set to false by any critical aspect
healthy = True
healthcheck_interval_sec = 5.   # seconds

# hilbert heartbeat
heartbeat_enabled = True
heartbeat_startup_sec = 30.  # seconds
heartbeat_interval_sec = max(healthcheck_interval_sec, 10.)   # seconds
heartbeat_shutdown_sec = 10.  # seconds
heartbeat_first_ping_shown = False

# programs to launch, and their parameters
launch = {
    "compton": {
        "cmd": "compton --vsync opengl".split(),
        "cwd": "./",
        "proc": None,
        "enabled": True,
        "monitor": True,
        },
    "bonsai": {
        "cmd": "runtime/build/bonsai2_slowdust --war-of-galaxies ../galaxy_data/available/ --port {port} -i ../galaxy_data/dummy-color.tipsy --fullscreen --camera-distance 250".format(port=bonsai_port).split(),
        "cwd": "../Bonsai/",
        "proc": None,
        "enabled": True,
        "monitor": True,
        },
    "gui": {
        "cmd": "../WebGL/webkit/qtwebkit -l ../WebGL/webgl_GalaxyCollision.html -z 0.4".split(),
        "cwd": "./",
        "proc": None,
        "enabled": True,
        "monitor": True,
        },
    "xdotool": {
        "cmd": "xdotool search --sync Bonsai windowmove 0 0 windowsize 100% 100%".split(),
        "cwd": "./",
        "proc": None,
        "enabled": True,
        "monitor": False,
        },
    }
