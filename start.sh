#!/bin/sh

# color X background to indicate startup...
#xsetroot -solid darkgreen

echo "Enabling antialiasing"
#nvidia-settings --assign FXAA=1
nvidia-settings --assign FSAA=5 --assign FSAAAppControlled=0 --assign FSAAAppEnhanced=0

# need this to enable transparency
unset XLIB_SKIP_ARGB_VISUALS

# hide the mouse cursor
# jitter value hides it even for large movements
unclutter -idle 0 -jitter 4000 -root &

# include hilbert-heartbeat python module
export PYTHONPATH=${PYTHONPATH:-/I/hilbert-heartbeat/client/python/}

# change directory (or give full script path below)
wd="$( dirname $(readlink -e $0) )"
cd "$wd"

# script
exec venv-python.sh server.py

