# 0000 - Bonsai Server

## Short Description

This is a helper application for the 
[Galaxy Collision](https://gitlab.com/HITS_Supernova/1006_galaxycollision)
application and can *not* be run alone. It takes care of starting the necessary
components (GUI and Bonsai), relays messages between them, monitors their
health and reports the health status of the overall application back to the
Hilbert management system as "heartbeat".

This application is used at the [ESO Supernova Planetarium and Visitor
Centre](https://supernova.eso.org/?lang=en), Garching b. München.  For more
details about the project and how applications are run and managed within the
exhibition please see [this link](https://gitlab.com/HITS_Supernova/overview).   

## Requirements / How-To

This application can not be run alone, it is meant to be used as git submodule
in the context of the [Galaxy Collision](https://gitlab.com/HITS_Supernova/1006_galaxycollision)
application.

Software: requires at least Python 3.4 and additional modules installed (e.g.
via `pip`): tornado (known working: 3.1.1), psutil (known working: 1.2.1) and
[hilbert-heartbeat](https://github.com/hilbert/hilbert-heartbeat/blob/master/client/python/heartbeat.py)
functions for import.


## Detailed Information

Configuration is done in `cfg.py` and script should be started with `python server.py`. 
`start.sh` is a wrapper script we're using for this.

## Credits and License

Developed by Volker Gaibler, HITS gGmbH. This repository is licensed under the [MIT license](LICENSE).

