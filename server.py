#!/usr/bin/env python3

import sys

import logging
# https://docs.python.org/2/library/logging.html#logrecord-attributes
#fmt = "%(asctime)s [%(levelname)-7s %(name)s:%(funcName)20s:%(lineno)3d]  %(message)s"
fmt = "%(asctime)s [%(levelname)-7s] %(message)s"
logging.basicConfig(
        format=fmt,
        stream=sys.stdout,
        datefmt="%Y-%m-%d %H:%M:%S",
        #datefmt="%H:%M:%S",
        #filename="log.txt",
        )
#logger = logging.getLogger(__name__)
logger = logging.getLogger("server")

# external commands
import subprocess
import psutil

# websockets
import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web

# network (to bonsai)
import socket

# other
import datetime
import time
import json

import argparse

# signals?
import signal
import os

# hilbert-heartbeat library reachable via PYTHONPATH
# warning: import adds a logger and already configures it with logging.basicConfig,
# so we need to do this _before_ importing heartbeat
# (this will be fixed with hilbert-heartbeat commit e305bf6655b5f6e7504550b50227ee700b3c28b7)
import heartbeat

# global config
# https://docs.python.org/3/faq/programming.html#how-do-i-share-global-variables-across-modules
#
import cfg

###############################################################################
#
class WSHandler(tornado.websocket.WebSocketHandler):

    ping_pong_enabled = True

    def open(self):
        logger.info("WS User connected to websocket server: {}".format(self.request.host))
        #logger.info(repr(self.request)

        # particle number in dummy mode
        if cfg.dummy_mode:
            self.dummy_particles = [0, 0, 0, 0]
            self.dummy_time0 = time.time()

    def on_message(self, message):
        logger.debug("WS Received message from client: '{}'".format(message))
        now = datetime.datetime.now()

        # relay message to Bonsai
        #
        if not cfg.dummy_mode:
            recv = cfg.commander.send_command(message)

        # relay answer back to gui
        #
        if not cfg.dummy_mode:
            ### real functionality
            logger.debug("WS Sending message to client: '{}'".format(recv))
            self.write_message(recv)

        else:
            ### dummy mode
            logger.warning("WS in dummy mode")
            ws_in = json.loads(message)
            if ws_in["task"] == "release":
                self.dummy_particles[ws_in["user_id"]] += 44444
                r = {
                    "response": ws_in["task"],
                    "simulation_time": round(time.time() - self.dummy_time0, 1),
                    "user_particles": self.dummy_particles,
                    }
            elif ws_in["task"] == "report":
                r = {
                    "response": ws_in["task"],
                    "simulation_time": round(time.time() - self.dummy_time0, 1),
                    "user_particles": self.dummy_particles,
                    }
            elif ws_in["task"] == "remove":
                self.dummy_particles[ws_in["user_id"]] = 0
                r = {
                    "response": ws_in["task"],
                    "simulation_time": round(time.time() - self.dummy_time0, 1),
                    "user_particles": self.dummy_particles,
                    }
            else:
                r = {
                    "response": "Error - could not handle ws input with dummy server",
                    }

            r["dummydata"] = True
            json_string = json.dumps(r)
            logger.info("WS dummy message to client: {}".format(json_string))
            self.write_message(json_string)

        # playing: send a ping with every message
        if self.ping_pong_enabled:
            msg = "howdy".encode("utf-8")
            self.ping(msg)
            logger.debug("WS Sent a ping to the client: {}".format(msg))

    def on_close(self):
        logger.info("WS Connection closed\n")

    def on_pong(self, msg):
        if self.ping_pong_enabled:
            logger.debug("WS Received a pong from the client: {}".format(msg))


###############################################################################
#
class BonsaiCommander(object):
    """Communication object to communicate with Bonsai via socket.

    The optional exit_callback is called to shutdown upon fatal error or end-of-life.
    """

    def __init__(self, host, port):
        self._server_ip = host
        self._server_port = port
        self._socket = None
        self._socket_timeout = cfg.bonsai_socket_timeout
        self.connected = False
        self.report_counter = 0
        self.show_report_every = 100

        logger.info("BC init for host {} and port {}.".format(
            self._server_ip, self._server_port))

        # for simplicity, we connect on creation
        # but this could also be delayed and re-connection allowed....
        if not cfg.dummy_mode:
            self.connect()
            logger.info("BC init done.")


    def connect(self):
        """Establish connection to Bonsai."""

        t0 = time.time()
        logger.info("BC connecting to bonsai.")
        logger.info("This may take a while, depending on bonsai startup speed...")

        while not self.connected:
            try:
                self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self._socket.connect((self._server_ip, self._server_port))
                self._socket.settimeout(self._socket_timeout)
            except socket.error as e:
                if self._socket:
                    self._socket.close()
                logger.info("BC Could not yet connect to {}, port {}: {}".format(
                    self._server_ip, self._server_port, e))
                if time.time() - t0 < cfg.bonsai_connect_retry_maxtime:
                    logger.info("BC Retrying...")
                    time.sleep(cfg.bonsai_connect_retry_sleep)
                else:
                    logger.info("BC Giving up.")
                    raise
            else:
                self.connected = True
                logger.info("BC established connection:")
                logger.info("BC    my socket: {}".format(self._socket.getsockname()))
                logger.info("BC    remote socket: {}".format(self._socket.getpeername()))


    def terminate(self):
        """Shutdown or terminate on error."""

        if self._socket:
            self._socket.shutdown(socket.SHUT_RDWR)
            self._socket.close()
        self.connected = False


    def send_command(self, send_json):
        """Send data to Bonsai, received an answer and return it to caller."""

        report_string = '{ "task": "report" }'

        # sparse logging of "report" messages (avoid excessive logging)
        sparse_msg = ""
        if send_json == report_string:
            self.report_counter += 1
            if self.report_counter % self.show_report_every == 0:
                log_ref = logger.info
                sparse_msg = "   <only every {}th>".format(self.show_report_every)
            else:
                log_ref = logger.debug
        else:
            log_ref = logger.info

        # send to Bonsai
        log_ref("BC send to bonsai: {}{}".format(send_json, sparse_msg))
        try:
            self._socket.send(send_json.encode("utf-8"))
        except:
            logger.error("BC send timeout. Bonsai hanging?")

        # receive from Bonsai
        logger.debug("BC calling recv ...")
        try:
            recv_json = self._socket.recv(cfg.bonsai_buffersize)
        except:
            logger.error("BC recv timeout ({} seconds). Bonsai hanging?".format(
                self._socket_timeout))
            recv_json = b"{}"
            cfg.healthy = False  # will trigger shutdown; but could also just stop with exception?
        else:
            log_ref("BC received from bonsai: {}{}".format(recv_json, sparse_msg))

        return recv_json


###############################################################################
# SUBROUTINES
###############################################################################

def sig_handler_sigchld(signum, frame):
    """Signal handler for SIGCHLD

    Check whether necessary processes are still running.
    Use p.status instead of p.is_running(), because is_running() also catches includes zombies,
    """
    logger.info("SIGCHLD received: child process died?")

    for key in cfg.launch.keys():
        if (cfg.launch[key]["proc"] and
                cfg.launch[key]["monitor"] and
                cfg.launch[key]["proc"].status not in (psutil.STATUS_RUNNING, psutil.STATUS_SLEEPING)):
            logger.info("Fatal: %s is dead.", key)
            #logger.info("   {key}  {proc}  {status}".format(
            #   key=key, proc=cfg.launch[key]["proc"], status=cfg.launch[key]["proc"].status))
            #psutil.Popen("ps faux".split(),
            #    cwd=".")
            cfg.healthy = False
            raise RuntimeError("%s is dead." % key)
        else:
            logger.info("%s: is ok.", key)

    # get here only if all alive, no RuntimeError
    logger.info("SIGCHLD: nothing happened. Ignoring.")


def sig_handler_exit(signum, frame):
    logger.info("Signal handler here: called with signal %d", signum)
    logger.info("Could terminate cleanly now...")
    raise KeyboardInterrupt  # treat SIGTERM just as Ctrl-C (hack!)


def set_signal_handler():
    logger.debug("Setting up signal handlers...")
    signal.signal(signal.SIGTERM, sig_handler_exit)
    #signal.signal(signal.SIGINT, sig_handler_exit)
    signal.signal(signal.SIGCHLD, sig_handler_sigchld)


def clean_up_and_stop():
    logger.info("Cleaning up for shutdown...")

    # stop tornado:
    # https://stackoverflow.com/questions/5375220/how-do-i-stop-tornado-web-server
    if cfg.commander:
        logger.info("Terminating commander...")
        cfg.commander.terminate()
    logger.info("Terminating tornado...")
    ioloop = tornado.ioloop.IOLoop.instance()
    ioloop.add_callback_from_signal(ioloop.stop)
    #ioloop.add_callback(ioloop.stop)

    # terminate child processes
    for key in cfg.launch.keys():
        if cfg.launch[key]["proc"] and cfg.launch[key]["proc"].is_running():
            logger.info("Terminating %s...", key)
            cfg.launch[key]["proc"].terminate()

    if cfg.heartbeat_enabled:
        t = int(cfg.heartbeat_shutdown_sec * 1000)
        logger.info("Heartbeat: hb_done({})".format(t))
        heartbeat.hb_done(t)

    logger.info("Clean-up done.")


def heartbeat_setup_init():
    t = int(cfg.heartbeat_startup_sec * 1000)
    logger.info("Heartbeat: hb_init({})".format(t))
    heartbeat.hb_init(t)


def periodic_health_check():
    """Check healthy flag for bonsai (hanging communication?) and send hilbert heartbeat."""

    logger.debug("Bonsai healthy: {}".format(cfg.healthy))
    if not cfg.healthy:
        logger.error("Bonsai healthy: {}  --> Abort.".format(cfg.healthy))
        clean_up_and_stop()

    if cfg.heartbeat_enabled:
        t = int(cfg.heartbeat_interval_sec * 1000)
        if cfg.heartbeat_first_ping_shown:
            logger.debug("Heartbeat: hb_ping({})".format(t))
        else:
            logger.info("Heartbeat: hb_ping({})".format(t))
            logger.info("All further hb_ping() calls only shown on debug log level.")
            cfg.heartbeat_first_ping_shown = True
        heartbeat.hb_ping(t)


def start_tornado_server():
    logger.info("Starting up websocket server on port %d", cfg.wsport)

    logger.info("Registering periodic bonsai health check".format(cfg.healthy))
    h = tornado.ioloop.PeriodicCallback(periodic_health_check, cfg.healthcheck_interval_sec * 1000)
    h.start()

    application = tornado.web.Application([(r'/ws', WSHandler),])
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(cfg.wsport)
    tornado.ioloop.IOLoop.instance().start()   # runs forever!


def launch_it(config, name):
    """Start program in background and save reference to it, configured in "config" dictionary."""

    if config[name]["enabled"]:
        #if os.path.isfile(path) and os.access(path, os.X_OK):
        logger.debug("launch_it: starting up %s in the background", name)
        logger.debug("           " + " ".join(config[name]["cmd"]))
        proc = psutil.Popen(
            config[name]["cmd"],
            cwd=config[name]["cwd"]
            )
        config[name]["proc"] = proc
    else:
        logger.info("Skip launching of '{}': not enabled.".format(name))


def setup_logging(level="info"):
    numeric_level = getattr(logging, level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: %s" % level)
    logger.setLevel(numeric_level)
    logger.debug("Set log level to %s" % level.upper())


def parse_arguments():
    """Parsing of command line arguments."""

    parser = argparse.ArgumentParser(description="Galaxy Collision Server")
    parser.add_argument("-n", "--without-compton", action="store_true",
                        dest="without_compton",
                        help="Don't launch Compton")
    #parser.add_argument("--bonsai-port", action="store",
    #                    dest="bonsai_port",
    #                    default=50008,
    #                    help="Connect to Bonsai on this port")
    parser.add_argument("-l", "--loglevel", action="store",
                        dest="loglevel",
                        default="info",
                        help="set log level (error, warning, info, debug)")
    return parser.parse_args()



###############################################################################
# MAIN
###############################################################################

if __name__ == "__main__":

    args = parse_arguments()
    setup_logging(level=args.loglevel)

    logger.info("=============== Galaxy Collision Server ===============")
    logger.info("Command line arguments: {}".format(args))

    if cfg.heartbeat_enabled:
        heartbeat_setup_init()

    if args.without_compton:
        cfg.launch["compton"]["enabled"] = False

    try:
        # signal handling
        # needed for SIGCHLD anyway, so use this instead of simple try-except in main
        set_signal_handler()

        # launch compositor
        logger.info("Starting up compton...")
        launch_it(cfg.launch, "compton")

        # launch bonsai
        if not cfg.dummy_mode:
            logger.info("Starting up bonsai...")
            launch_it(cfg.launch, "bonsai")   # bonsai
            launch_it(cfg.launch, "xdotool")  # xdotool maximizes bonsai
        else:
            logger.warning("-------- dummy mode enabled in cfg.py --------------")

        # connection to Bonsai
        cfg.commander = BonsaiCommander(cfg.bonsai_host, cfg.bonsai_port)

        # launch gui
        if not cfg.dummy_mode:
            logger.info("Starting up gui...")
            launch_it(cfg.launch, "gui")

        # everlasting server loop
        start_tornado_server()

    except:
        logger.warning("Exception in main(): see traceback.", exc_info=True)

    finally:    # do in any case
        clean_up_and_stop()

# set return value
if cfg.healthy:
    logger.info("Exit code 0")
    sys.exit(0)
else:
    logger.info("Exit code: 1")
    sys.exit(1)
